AdsMoGo-iOS-SDK
============

AdsMoGo iOS 版本 V 1.4.9

为了减小仓库大小，每次都只保留最新版本。

如果要使用历史版本，请访问 [https://github.com/jcccn/AdsMoGo-iOS-SDK](https://github.com/jcccn/AdsMoGo-iOS-SDK)

------

* 依赖的框架
 